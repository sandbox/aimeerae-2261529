<?php
/**
 * @file
 * Form actions for SauceLabs Tests.
 */

/**
 * Form actions for Tests for the SauceLabs module.
 */
class SaucelabsTestForms {

  static function adminForm(&$form) {
    $form['saucelabs_settings'] = array(
      '#type' => 'container',
      '#weight' => 0,
      '#name' => 'SauceLab Settings',
    );
    $form['system_settings'] = array(
      '#type' => 'container',
      '#weight' => 1,
      '#name' => 'System Settings',
    );
    $form['system_settings']['saucelabs_vendor_location'] = array(
      '#type' => 'textfield',
      '#title' => t('Vendor Location'),
      '#description' => t('Directory in the Drupal root where vendor directory lives. eg: private/saucelabs/vendor'),
      '#default_value' => variable_get('saucelabs_vendor_location', ''),
      '#required' => TRUE,
    );
    $form['system_settings']['saucelabs_test_location'] = array(
      '#type' => 'textfield',
      '#title' => t('Test Location'),
      '#description' => t('Directory in the Drupal root where your test files live.'),
      '#default_value' => variable_get('saucelabs_test_location', ''),
      '#required' => TRUE,
    );
    $form['system_settings']['saucelabs_test_environments'] = array(
      '#type' => 'textarea',
      '#title' => t('Test Environments'),
      '#description' => t('The environments available to run tests on (Prod, Dev, etc). Format as [baseurl]|[name]. eg: http://mytestsite.com|Production'),
      '#default_value' => variable_get('saucelabs_test_environments', ''),
      '#required' => TRUE,
    );
    $form['system_settings']['saucelabs_parallel_threads'] = array(
      '#type' => 'textfield',
      '#title' => t('Number of Parallel threads'),
      '#description' => t('The number of concurrent threads to run tests on.'),
      '#default_value' => variable_get('saucelabs_parallel_threads', 1),
      '#required' => TRUE,
    );
    $form['system_settings']['saucelabs_php_binary_location'] = array(
      '#type' => 'textfield',
      '#title' => t('PHP 5.6 Binary Location'),
      '#description' => t('If you are overriding your default PHP to get PHP 5.6, you may need to include the PHP path here, for the shell_exec to find PHP 5.6. Eg. /Applications/MAMP/bin/php/php5.6.10/bin'),
      '#default_value' => variable_get('saucelabs_php_binary_location', ''),
      '#required' => FALSE,
    );
    $form['saucelabs_settings']['saucelabs_username'] = array(
      '#type' => 'textfield',
      '#title' => t('SauceLabs Username'),
      '#description' => t('Your SauceLabs account username.'),
      '#default_value' => variable_get('saucelabs_username', ''),
      '#required' => TRUE,
    );
    $form['saucelabs_settings']['saucelabs_api'] = array(
      '#type' => 'textfield',
      '#title' => t('SauceLabs Access Key'),
      '#description' => t('Your SauceLabs account access key.'),
      '#default_value' => variable_get('saucelabs_api', ''),
      '#required' => TRUE,
    );
  }

  static function testForm(&$form, $test) {
    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Test Name'),
      '#description' => t('Administrative name for the tests being run.'),
      '#default_value' => $test->getName(),
      '#required' => TRUE,
    );
    $form['description'] = array(
      '#type' => 'textarea',
      '#title' => t('Description'),
      '#description' => t('Description of the tests being run.'),
      '#default_value' => $test->getDescription(),
      '#required' => TRUE,
    );
    $selected_tests = array_combine($test->getTests(), array_fill(0, count($test->getTests()), TRUE));
    $form['tests'] = array(
      '#type' => 'tableselect',
      '#prefix' => t('<label>Available Tests</label>'),
      '#options' => SaucelabsTest::getTestFilesTableSelect(),
      '#header' => SaucelabsTest::getTestFilesHeaders(),
      '#empty' => t('No test files found.'),
      '#default_value' => $selected_tests,
    );
    $env_options = variable_get('saucelabs_test_environments', 'local|local');
    //use the List Module to convert the list of environments into an array
    $env_options_array = list_extract_allowed_values($env_options, 'list_text', FALSE);
    $form['environment'] = array(
      '#type' => 'select',
      '#title' => t('Environment'),
      '#options' => $env_options_array,
      '#description' => t('Choose which environment you wish to run this test on. Environments are configured in the Settings tab.'),
      '#required' => TRUE,
      '#default_value' => $test->getEnvironment(),
    );
    $form['path'] = array(
      '#type' => 'textfield',
      '#title' => t('Path'),
      '#description' => t('The path within the environment to run the test on.'),
      '#default_value' => $test->getPath(),
    );
    $form['report'] = array(
      '#type' => 'checkbox',
      '#title' => t('Report to Watchdog'),
      '#description' => t('Do you wish to have this test report each outcome to Watchdog?'),
      '#default_value' => $test->isReport(),
    );
    $form['test_id'] = array(
      '#type' => 'hidden',
      '#value' => $test->getTestId(),
    );
    $submit_text = t('Create Test');
    if (is_numeric($test->getTestId())) {
      $submit_text = t('Update Test');
    }
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => $submit_text,
    );

  }

  static function testFormValidate($form, $form_state) {
    $values = $form_state['values'];
    SaucelabsTest::validate($values);
  }

  static function testFormSubmit($form, &$form_state) {
    $values = $form_state['values'];
    SaucelabsTest::store($values);
    $form_state['redirect'] = 'admin/saucelabs/dashboard';
  }

}
