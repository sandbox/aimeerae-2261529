<?php
/**
 * @file
 * General methods for SauceLabs Tests.
 */

/**
 * General methods for Tests for the SauceLabs module.
 */
class SaucelabsTest {

  protected $test_id = NULL;
  protected $name = '';
  protected $description = '';
  protected $tests = array();
  protected $environment = '';
  protected $path = '';
  protected $report = TRUE;
  protected $last_test = 0;
  protected $last_duration = 0;
  protected $last_state = FALSE;
  protected $session_ids = array();

  /**
   * @return null
   */
  public function getTestId() {
    return $this->test_id;
  }

  /**
   * @param null $test_id
   */
  public function setTestId($test_id) {
    $this->test_id = $test_id;
  }

  public function isStoredTest() {
    if (is_numeric($this->getTestId())) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param string $name
   */
  public function setName($name) {
    $this->name = check_plain($name);
  }

  /**
   * @return string
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * @param string $description
   */
  public function setDescription($description) {
    $this->description = check_plain($description);
  }

  /**
   * @return array
   */
  public function getTests() {
    return $this->tests;
  }

  public function getFullTestPaths() {
    $location = variable_get('saucelabs_test_location', '');
    $tests = $this->getTests();
    foreach ($tests as &$test) {
      $test = $location . '/' . $test;
    }
    return $tests;
  }

  /**
   * @return string
   */
  public function getTestsSerialized() {
    return serialize($this->tests);
  }

  /**
   * @return string
   */
  public function getTestsFormatted() {
    return implode(', ', $this->tests);
  }

  /**
   * @param array $tests
   */
  public function setTests($tests) {
    $tests = array_filter($tests);
    $this->tests = $tests;
  }

  /**
   * @param $tests_string
   */
  public function setTestsSerialized($tests_string) {
    $this->setTests(unserialize($tests_string));
  }

  /**
   * @return string
   */
  public function getEnvironment() {
    return $this->environment;
  }

  /**
   * @return string
   */
  public function getFullEnvironment() {
    if (substr($this->environment, 0, 4) !== 'http') {
      return 'http://' . $this->environment;
    }
    return $this->environment;
  }

  /**
   * @param string $environment
   */
  public function setEnvironment($environment) {
    $this->environment = check_plain($environment);
  }

  /**
   * @return string
   */
  public function getPath() {
    return $this->path;
  }

  /**
   * @param string $path
   */
  public function setPath($path) {
    $this->path = check_plain($path);
  }

  public function getFullPath() {
    return $this->getFullEnvironment() . '/' . $this->getPath();
  }

  /**
   * @return boolean
   */
  public function isReport() {
    return $this->report;
  }

  /**
   * @param boolean $report
   */
  public function setReport($report) {
    $this->report = $report;
  }

  /**
   * @return int
   */
  public function getLastTest() {
    return $this->last_test;
  }

  /**
   * @param int $last_test
   */
  public function setLastTest($last_test) {
    $this->last_test = $last_test;
  }

  /**
   * @return int
   */
  public function getLastDuration() {
    return $this->last_duration;
  }

  /**
   * @param int $last_duration
   */
  public function setLastDuration($last_duration) {
    $this->last_duration = $last_duration;
  }


  /**
   * @return boolean
   */
  public function isLastState() {
    return $this->last_state;
  }

  /**
   * Returning value for DB
   * @return boolean
   */
  public function getLastState() {
    return ($this->last_state) ? 1 : 0;
  }

  public function getStateText() {
    return ($this->isLastState()) ? t('Passed') : t('Failed');
  }

  /**
   * @param boolean $last_state
   */
  public function setLastState($last_state) {
    $this->last_state = $last_state;
  }

  /**
   * @return array
   */
  public function getSessionIds() {
    return $this->session_ids;
  }

  public function getSessionIdsSerialized() {
    return serialize($this->session_ids);
  }

  /**
   * @param array $session_ids
   */
  public function setSessionIds($session_ids) {
    $this->session_ids = $session_ids;
  }

  public function addSessionIdsFromFile($session_ids_string) {
    if (strlen($session_ids_string) > 0) {
      $session_ids = explode(PHP_EOL, $session_ids_string);
      $session_ids = array_merge($this->getSessionIds(), $session_ids);
      $this->setSessionIds($session_ids);
    }
  }

  public function setSessionIdsFromFile($session_ids_string) {
    if (strlen($session_ids_string) > 0) {
      $session_ids = explode(PHP_EOL, $session_ids_string);
      $this->setSessionIds($session_ids);
    }
  }

  public function setSessionIdsSerialized($session_ids_string) {
    $this->setSessionIds(unserialize($session_ids_string));
  }

  public function __construct($test = array(), $source = 'db') {
    if (count($test)) {
      switch ($source) {
        case 'db':
          $this->buildFromDBArray($test);
          break;
        case 'form':
          $this->buildFromFormState($test);
          break;
      }

    }
  }

  protected function buildFromDBArray($test_array) {
    $this->setTestId($test_array['test_id']);
    $this->setName($test_array['name']);
    $this->setDescription($test_array['description']);
    $this->setTestsSerialized($test_array['tests']);
    $this->setEnvironment($test_array['environment']);
    $this->setPath($test_array['path']);
    $this->setReport($test_array['report']);
    $this->setLastTest($test_array['last_test']);
    $this->setLastDuration($test_array['last_duration']);
    $this->setLastState($test_array['last_state']);
    $this->setSessionIdsSerialized($test_array['session_ids']);
  }

  protected function buildFromFormState($test_array) {
    $this->setName($test_array['name']);
    $this->setDescription($test_array['description']);
    $this->setTests($test_array['tests']);
    $this->setEnvironment($test_array['environment']);
    $this->setPath($test_array['path']);
    $this->setReport($test_array['report']);
  }

  public function updateFromFormState($test) {
    $this->buildFromFormState($test);
  }

  public function save() {
    SaucelabsTestDB::merge($this);
  }

  public function getRecord() {
    $record = array(
      'name' => $this->getName(),
      'description' => $this->getDescription(),
      'environment' => $this->getEnvironment(),
      'tests' => $this->getTestsSerialized(),
      'path' => $this->getPath(),
      'report' => $this->isReport(),
      'last_test' => $this->getLastTest(),
      'last_duration' => $this->getLastDuration(),
      'last_state' => $this->getLastState(),
      'session_ids' => $this->getSessionIdsSerialized(),
    );
    return $record;
  }

  public function getListingRecord() {
    $last_state = $this->getStateText();
    $last_duration = $this->getLastDuration();
    if (($last_state === t('Failed')) && ($last_duration == 0)) {
      $last_state = t('Never Run');
      $last_duration = t('Never Run');
    }
    $name_options = array(
      'attributes' => array(
        'title' => $this->getDescription(),
      ),
    );
    $record = array(
      'name' => l($this->getName(), 'admin/saucelabs/tests/' . $this->getTestId(), $name_options),
      'environment' => $this->getEnvironment(),
      'path' => $this->getPath(),
      'tests' => $this->getTestsFormatted(),
      'last_state' => $last_state,
      'last_duration' => $last_duration,
    );
    return $record;
  }

  public static function getListingHeader() {
    return array(
      'name' => t('Name'),
      'environment' => t('Environment'),
      'path' => t('Path'),
      'tests' => t('Tests'),
      'last_state' => t('Last State'),
      'last_duration' => t('Last Duration (seconds)'),
    );
  }

  public static function buildFromId($test_id) {
    $test_array = SaucelabsTestDB::query($test_id);
    $test = new SaucelabsTest($test_array);
    return $test;
  }

  public static function finishedBatchRun($test_id) {
    $test = self::buildFromId($test_id);
    $test->storeTempSessionData();
    $test->setDataFromSaucelabs();
    $test->save();
    $test->notifyTestRun();
  }

  protected function notifyTestRun() {
    $this->notifyWatchdog();
    module_invoke_all('saucelabs_test_complete', $this);
  }

  protected function notifyWatchdog() {
    if ($this->isReport()) {
      $name = $this->getName();
      $status = $this->getStateText();
      $severity = WATCHDOG_NOTICE;
      if($status === 'Failed') {
        $severity = WATCHDOG_WARNING;
      }
      watchdog('SauceLabs', 'Test titled %test_name has %status', array(
        '%test_name' => $name,
        '%status' => $status
      ), $severity);
    }
  }

  /**
   * Stores Temporary Session data, currently in files in DRUPAL TEMP, into the
   * object, and later into db.
   * Due to the fact that multiple files might be generated (if there is more
   * than one test), we wipe the session ids at the start and use
   * 'addSessionIdsFromFile' so as to avoid just storing the last file read.
   */
  protected function storeTempSessionData() {
    $this->setSessionIds(array());
    foreach ($this->getFullTestPaths() as $testFile) {
      $temp_storage = $this->getTempFilePath($testFile);
      try {
        $handle = fopen($temp_storage, "r");
        if (!$handle) {
          watchdog('SauceLabs', 'Temporary Storage not available.');
        }
        else {
          $contents = fread($handle, filesize($temp_storage));
          fclose($handle);
          $this->addSessionIdsFromFile($contents);
        }
      } catch (Exception $e) {
        watchdog('SauceLabs', $e->getMessage());
      }
    }
  }

  protected function setDataFromSaucelabs() {
    $session_ids = $this->getSessionIds();
    $provisional_state = TRUE;
    $provisional_duration = 0;
    $provisional_start = 0;
    if (count($session_ids) > 0) {
      foreach ($session_ids as $session_id) {
        if (strlen($session_id)) {
          $s = self::getSaucelabsApi();
          if (isset($s)) {
            $job = $s->getJob($session_id);
            if (!$job['passed']) {
              $provisional_state = FALSE;
            }
            if ($job['creation_time'] < $job['end_time']) {
              $provisional_duration += ($job['end_time'] - $job['creation_time']);
            }
            else {
              if ($job['start_time'] < $job['end_time']) {
                $provisional_duration += ($job['end_time'] - $job['start_time']);
              }
            }
            if (($provisional_start === 0) || ($job['creation_time'] < $provisional_start)) {
              $provisional_start = $job['creation_time'];
            }
          }
          else {
            drupal_set_message(t('Please enter your SauceLab details, including location of code, in the settings page, to get Test data back.'));
          }
        }
      }
      $this->setLastState($provisional_state);
      $this->setLastTest($provisional_start);
      $this->setLastDuration($provisional_duration);
    } else {
      drupal_set_message(t('No session ids were available, this test was not recorded.'), 'error');
      watchdog('SauceLabs', 'No session ids were available', array(), WATCHDOG_ERROR);
    }
  }

  public static function getSaucelabsApi() {
    $path_to_vendor = variable_get('saucelabs_vendor_location', NULL);
    if (isset($path_to_vendor)) {
      require_once($path_to_vendor . '/sauce/sausage/src/Sauce/Sausage/SauceAPI.php');
      require_once($path_to_vendor . '/sauce/sausage/src/Sauce/Sausage/SauceMethods.php');
      $username = variable_get('saucelabs_username', '');
      $api_key = variable_get('saucelabs_api', '');
      return new Sauce\Sausage\SauceAPI($username, $api_key);
    }
    return NULL;
  }

  public function getStoredSessionsEmbedJS() {
    $session_ids = $this->getSessionIds();
    $jsPaths = array();
    $username = variable_get('saucelabs_username', '');
    $api_key = variable_get('saucelabs_api', '');
    foreach ($session_ids as $session_id) {
      $auth = hash_hmac('md5', $session_id, $username . ':' . $api_key);
      $jsPaths[] = 'https://saucelabs.com/job-embed/' . $session_id . '.js?auth=' . $auth;
    }
    return $jsPaths;
  }

  protected static function getTestFiles() {
    $location = variable_get('saucelabs_test_location', FALSE);
    $options = array();
    // Find the tests files. Only test files should exist in this directory.
    if ($files = file_scan_directory(DRUPAL_ROOT . base_path() . $location, "/^Test.*\.php$/", array('recurse' => FALSE), 1)) {
      foreach ($files as $file) {
        $options[$file->uri] = $file->filename;
      }
    }
    return $options;
  }

  public static function getTestFilesTableSelect() {
    $files = array();
    foreach (self::getTestFiles() as $file_uri => $file_name) {
      $file_data_array = self::getTestFileDocBlock($file_uri);
      $file_data_array['filename'] = $file_name;
      $files[$file_name] = $file_data_array;
    }
    return $files;
  }

  public static function getTestFilesHeaders() {
    return $header = array(
      'filename' => t('Filename'),
      'title' => t('Title'),
      'description' => t('Description'),
    );
  }

  public static function validate($values) {
    $tests = array();
    foreach ($values['tests'] as $test_key => $test_file) {
      if ($test_key === $test_file) {
        $tests[] = $test_file;
      }
    }
    if (count($tests) === 0) {
      form_set_error('tests', 'Please choose at least one test from the test listing');
    }
  }

  /**
   * Store values from Form submission, data from $form_state['values']
   * which is passed in as an array.
   *
   * @param $values
   */
  public static function store($values) {
    if (isset($values['test_id']) && is_numeric($values['test_id'])) {
      $test = self::buildFromId($values['test_id']);
    }
    else {
      $test = new SaucelabsTest();
    }
    $test->updateFromFormState($values);
    $test->save();
  }

  public static function getAll() {
    $results = SaucelabsTestDB::listAll();
    $objectArray = array();
    foreach ($results as $row) {
      $objectArray[] = new SaucelabsTest($row);
    }
    return $objectArray;
  }


  /**
   * Get the first doc block from a test file, parse the content and return
   *
   * @param $file_uri
   * @return array
   */
  protected static function getTestFileDocBlock($file_uri) {
    $docComments = array_filter(
      token_get_all(file_get_contents($file_uri)), function ($entry) {
      return $entry[0] == T_DOC_COMMENT;
    }
    );
    $fileDocComment = array_shift($docComments);
    $docCommentContent = array_slice(explode(PHP_EOL, $fileDocComment[1]), 2, 3);
    $commentArray = array('title' => '', 'description' => '');
    $commentArrayMap = array(
      0 => 'className',
      1 => 'title',
      2 => 'description'
    );
    if (count($docCommentContent) > 0) {
      foreach ($docCommentContent as $i => $comment) {
        // Remove the ' * ' from the start of each comment line
        $pattern = '/^([\s]*\*[\s]*)/';
        $replacement = '';
        $comment = preg_replace($pattern, $replacement, $comment);
        $commentArray[$commentArrayMap[$i]] = $comment;
      }
    }
    return $commentArray;
  }

  protected function getEnvironmentVars() {
    $username = variable_get('saucelabs_username', '');
    $api_key = variable_get('saucelabs_api', '');
    $path = $this->getFullPath();
    $env_vars = array();
    $php_path = variable_get('saucelabs_php_binary_location', '');
    if (strlen($php_path)) {
      $env_vars[] = 'export PATH=' . $php_path . ':$PATH';
    }
    $env_vars[] = 'export SAUCE_USERNAME=' . $username;
    $env_vars[] = 'export SAUCE_ACCESS_KEY=' . $api_key;
    $env_vars[] = 'export SAUCE_PATH=' . $path;
    return $env_vars;
  }

  protected static function sanitizeFilePath($file_path) {
    if (!file_exists($file_path)) {
      $location = variable_get('saucelabs_test_location', FALSE);
      $file_path = $location . '/' . $file_path;
      if (file_exists($file_path)) {
        return $file_path;
      }
      return NULL;
    }
    return $file_path;
  }

  public static function getTempFilePath($test_file) {
    $test_file = self::sanitizeFilePath($test_file);
    if (isset($test_file)) {
      $class_data = self::getTestFileDocBlock($test_file);
      if (isset($class_data['className'])) {
        $class_name = $class_data['className'];
      }
      else {
        $path_parts = explode('/', $test_file);
        $class_name = end($path_parts);
      }
      return file_directory_temp() . '/' . $class_name . '.sessions';
    }
    return FALSE;
  }

  public function run($redirect = FALSE) {
    try {
      $path_to_vendor = variable_get('saucelabs_vendor_location', '');
      $threads = variable_get('saucelabs_parallel_threads', 1);

      $env_vars = $this->getEnvironmentVars();
      $jobs = array();
      foreach ($this->getFullTestPaths() as $test_file) {
        $temp_storage = self::getTempFilePath($test_file);
        $env_vars[] = 'export SAUCE_TMP_SESSION_STORAGE=' . $temp_storage;
        fopen($temp_storage, 'w');
        $prefix = implode(';', $env_vars);
        $exec_string = $path_to_vendor . '/bin/paratest -p ' . $threads . ' -f --phpunit=' . $path_to_vendor . '/bin/phpunit ' . $test_file . ' 2>&1';
        $jobs[] = $prefix . ';' . $exec_string;
      }

      if (count($jobs)) {
        // Define the Batch.
        // @see saucelabs.batch.inc for batch functions.
        $batch = array(
          'operations' => array(
            array('saucelabs_process_update', array($jobs, $this->getTestId())),
          ),
          'finished' => 'saucelabs_run_tests_finished',
          'title' => t('SauceLabs Batch'),
          'init_message' => t('Batch is starting.'),
          'progress_message' => t('Processed @current out of @total.'),
          'error_message' => t('SauceLabs Batch has encountered an error.'),
          'file' => drupal_get_path('module', 'saucelabs') . '/saucelabs.batch.inc',
        );
        batch_set($batch);
        if ($redirect) {
          batch_process('admin/saucelabs/tests/' . $this->getTestId());
        }
      }
    } catch (Exception $e) {
      watchdog('SauceLabs', $e->getMessage(), WATCHDOG_ERROR);
    }

  }

}
