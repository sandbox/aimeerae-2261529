<?php
/**
 * @file
 * Database methods for SauceLabs Tests.
 */

/**
 * Database methods for Tests for the SauceLabs module.
 */
class SaucelabsTestDB {
  /**
   * Updates or inserts a new record.
   *
   * @param $saucelabsTest SaucelabsTest object.
   */
  public static function merge($saucelabsTest) {
    if ($saucelabsTest->isStoredTest()) {
      db_merge('saucelabs')
        ->key(array(
          'test_id' => $saucelabsTest->getTestId(),
        ))
        ->fields($saucelabsTest->getRecord())
        ->execute();
    }
    else {
      db_insert('saucelabs')
        ->fields($saucelabsTest->getRecord())
        ->execute();
    }
  }

  public static function delete($test_id) {
    db_delete('saucelabs')
      ->condition('test_id', $test_id)
      ->execute();
  }

  /**
   * Get the SauceLabs Tests from the database.
   *
   * @param $test_id integer test_id for the Saucelab Test.
   * @return int|boolean An integer representing the language fallback setting
   *  or FALSE if the record does not exist.
   */
  public static function query($test_id) {
    $select = db_select('saucelabs');
    $select->fields('saucelabs');
    $select->condition('test_id', $test_id);
    return $select->execute()->fetchAssoc();
  }

  public static function listAll() {
    $select = db_select('saucelabs');
    $select->fields('saucelabs');
    return $select->execute()->fetchAllAssoc('test_id', PDO::FETCH_ASSOC);
  }

}
