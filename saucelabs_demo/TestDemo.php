<?php

/**
 * @file
 * TestDemo
 * Test Demo
 * Demo of how a Test file should be set up.
 */

require_once 'SauceLabsCoreTest.php';

class TestDemo extends SauceLabsCoreTest {
  protected $start_url = SAUCE_PATH;

  public static $browsers = array(
    // run FF15 on Windows 8 on Sauce
    array(
      'browserName' => 'firefox',
      'desiredCapabilities' => array(
        'version' => '15',
        'platform' => 'Windows 2012',
      )
    ),
    // run Chrome on Linux on Sauce
    array(
      'browserName' => 'chrome',
      'desiredCapabilities' => array(
        'platform' => 'Linux'
      )
    ),
  );

  public function testDemoTest() {
    $this->assertContains("Some Page Title", $this->title());
  }

}
