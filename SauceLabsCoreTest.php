<?php

/**
 * @file
 * SauceLabs Core Test
 *
 */

define('SAUCE_PATH', getenv('SAUCE_PATH'));
define('SAUCE_TMP_SESSION_STORAGE', getenv('SAUCE_TMP_SESSION_STORAGE'));

require_once 'vendor/autoload.php';

class SauceLabsCoreTest extends Sauce\Sausage\WebDriverTestCase
{
  public function setUp() {
    $this->shareSession(true);
    parent::setUp();
    $this->setBrowserUrl(SAUCE_PATH);
  }

  public function prepareSession() {
    parent::prepareSession();
    file_put_contents(SAUCE_TMP_SESSION_STORAGE, $this->getSessionId() . PHP_EOL, FILE_APPEND);
  }

}
