SauceLabs Drupal Monitoring integration.

SauceLabs allows you to run tests (such as Selenium PHP tests in this case) via
their servers.

This module helps you integrate those tests with a front end, where you can
acquire EndPoints for the tests, to call them via CRON, and also set up
notifications or actions to happen based on the results.

1. Get a SauceLabs account:
Visit https://saucelabs.com and sign up if you haven't already

2. Download Sausage for whatever environment the code will be running on:
https://wiki.saucelabs.com/display/DOCS/Setting+Up+Sausage+for+OS+X+and+Linux
https://saucedev.atlassian.net/wiki/display/DOCS/Setting+Up+Sausage+for+Windows

In the case of Linux/OS X you would run:

curl -s https://raw.githubusercontent.com/jlipps/sausage-bun/master/givememysausage.php | php

(don't include Username and Key as this will be set up in the config)

3. Place the vendor folder within a directory of your choosing (eg. private/saucelabs).

4. Copy SauceLabsCoreTest.php.bak to this same folder (eg. private/saucelabs) and remove the '.bak'.

5. View saucelabs_demo/TestDemo.php to see the general structure of tests:
  i) All test files need to start with 'Test'.
  ii) All test files need to extend SauceLabsCoreTest.
  ii) All test files need to have a structured file docblock as follows:

  /**
    * @file
    * {ClassName}
    * {Name of Test}
    * {Description of Test}
  */

6. Place test file in same location as SauceLabsCoreTest.php

7. Install SauceLabs module and configure settings.

8. You should see a list of any Test files on the dashboard. From here you can store your own
test instances, and run tests straight away!

9. To see how Message Stack, or any other use of completion hooks can be integrated, install saucelabs_demo.
