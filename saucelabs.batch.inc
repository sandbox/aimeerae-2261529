<?php
/**
 * @file
 * Batch API definitions for the saucelabs module.
 */

/**
 * Process the batch.
 */
function saucelabs_process_update($jobs, $test_id = NULL, &$context) {
  $context['sandbox']['test_id'] = $test_id;

  $context['results']['test_id'] = $test_id;
  // Set starting values that track progress of the batch.
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = count($jobs);
    $context['sandbox']['currently_running'] = $jobs[$context['sandbox']['progress']];
  }

  $context['message'] = t('@count/@max | Running Test @testname', array(
      '@count' => $context['sandbox']['progress'],
      '@max' => $context['sandbox']['max'],
      '@testname' => $jobs[$context['sandbox']['progress']],
    )
  );

  // Run SauceLabs Test on current Test Command.
  $success = _saucelabs_run_test($jobs[$context['sandbox']['progress']]);
  // Update our progress information.
  if ($success) {
    $context['results']['jobs'][] = $jobs[$context['sandbox']['progress']] . " finished";
  }
  $context['sandbox']['progress']++;
  if (isset($jobs[$context['sandbox']['progress']])) {
    $context['sandbox']['currently_running'] = $jobs[$context['sandbox']['progress']];
  }

  // Inform the batch engine that we are not finished,
  // and provide an estimation of the completion level we reached.
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Batch 'finished' callback.
 */
function saucelabs_run_tests_finished($success, $results, $operations) {
  if ($success) {
    // Here we do something meaningful with the results.
    $message = count($results['jobs']) . ' processed.';
    $message .= theme('item_list', $results);
    if (isset($results['test_id'])) {
      SaucelabsTest::finishedBatchRun($results['test_id']);
    }
    else {
      drupal_set_message(t('Tests complete, please check on SauceLabs to view results'));
    }
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
        '%error_operation' => $error_operation[0],
        '@arguments' => print_r($error_operation[1], TRUE)
      )
    );
  }
  drupal_set_message($message);
}

/**
 * Run Test.
 *
 * @param string $exec_string
 *   Should be full executable string.
 *
 * @return bool
 */
function _saucelabs_run_test($exec_string) {
  if ($result = shell_exec($exec_string)) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}
