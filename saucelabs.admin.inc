<?php

/**
 * @file
 * Menu Callback definitions for the SauceLabs module.
 */

/**
 * Test Creation form.
 *
 * @param $form
 * @param $form_state
 * @param $test
 * @return mixed
 */
function saucelabs_edit_test_form($form, &$form_state, $test = NULL) {
  if ($test === NULL) {
    $test = new SaucelabsTest();
  }
  SaucelabsTestForms::testForm($form, $test);
  return $form;
}

/**
 * Test Creation validation
 *
 * @param $form
 * @param $form_state
 * @return mixed
 */
function saucelabs_edit_test_form_validate($form, &$form_state) {
  SaucelabsTestForms::testFormValidate($form, $form_state);
}

/**
 * Test Creation submission
 *
 * @param $form
 * @param $form_state
 * @return mixed
 */
function saucelabs_edit_test_form_submit($form, &$form_state) {
  SaucelabsTestForms::testFormSubmit($form, $form_state);
}

/**
 * SauceLabs Settings form.
 *
 * @param $form
 * @param $form_state
 */
function saucelabs_settings_form($form, &$form_state) {
  SaucelabsTestForms::adminForm($form);
  return system_settings_form($form);
}

/**
 * SauceLabs view Test information
 *
 * @param $form
 * @param $form_state
 * @param null $test
 * @return array
 */
function saucelabs_view_test($form, &$form_state, $test = NULL) {

  global $base_url;

  if (isset($test)) {
    if (isset($form_state['storage']) && ($form_state['storage']['delete'])) {

      $delete_confirm = t('Sure you want to delete %test_name?', array('%test_name' => $test->getName()));
      $form['intro'] = array(
        '#markup' => theme('html_tag', array(
          'element' => array(
            '#tag' => 'p',
            '#value' => $delete_confirm,
          ),
        ))
      );
      $form['test_id'] = array(
        '#type' => 'hidden',
        '#value' => $test->getTestId(),
      );
      $form['#submit'][] = 'saucelabs_test_delete_submit';
      return confirm_form($form, $question = "Confirm deletion of Test", 'admin/saucelabs/tests/' . $test->getTestId() . '/view');
    }

    $form = array();
    $prefix = theme('html_tag', array(
      'element' => array(
        '#tag' => 'h2',
        '#value' => $test->getName(),
      ),
    ));
    $prefix .= theme('html_tag', array(
      'element' => array(
        '#tag' => 'p',
        '#value' => $test->getDescription(),
      ),
    ));
    $current_details = array();
    $current_details[] = t('Runs: %tests', array('%tests' => implode(', ', $test->getTests())));
    $current_details[] = t('On Path: %path', array('%path' => $test->getFullPath()));
    $current_details[] = ($test->isReport()) ? t('Reporting to Watchdog') : t('Not reporting to Watchdog');
    $cron_key = variable_get('cron_key', 'drupal');
    $current_details[] = t('Direct Path for running test via Cron: <strong>!base_url/admin/saucelabs/tests/!saucelabs_test_id/run?cron_key=!cron_key</strong>', array('!base_url' => $base_url, '!saucelabs_test_id' => $test->getTestId(), '!cron_key' => $cron_key));
    $prefix .= theme('html_tag', array(
      'element' => array(
        '#tag' => 'p',
        '#value' => implode('</br>', $current_details),
      ),
    ));

    $last_test_details[] = t('Last Test Details:');
    $last_status = $test->getStateText();
    $status_colour = ($test->isLastState()) ? 'green' : 'red';
    $last_test_details[] = theme('html_tag', array(
      'element' => array(
        '#tag' => 'span',
        '#value' => $last_status,
        '#attributes' => array(
          'style' => 'color:' . $status_colour,
        ),
      ),
    ));
    $last_test_details[] = t('Started on: %date', array('%date' => date('d M Y H:i', $test->getLastTest())));
    $last_test_details[] = t('Duration: %duration seconds', array('%duration' => $test->getLastDuration()));
    $prefix .= theme('html_tag', array(
      'element' => array(
        '#tag' => 'p',
        '#value' => implode('</br>', $last_test_details),
      ),
    ));
    $form['#prefix'] = $prefix;
    $form['test_id'] = array(
      '#type' => 'hidden',
      '#value' => $test->getTestId(),
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Run Test'),
    );
    $form['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete Test'),
      '#submit' => array('saucelabs_test_delete_submit'),
    );
    $suffix = '';
    foreach ($test->getStoredSessionsEmbedJS() as $js_path) {
      $suffix .= '<script src="' . $js_path . '"></script>';
    }
    $form['#suffix'] = $suffix;
    return $form;
  }
  return NULL;
}

/**
 * SauceLabs submit test (run test)
 *
 * @param $form
 * @param $form_state
 */
function saucelabs_view_test_submit($form, &$form_state) {
  $test = SaucelabsTest::buildFromId($form_state['values']['test_id']);
  $test->run();
}

/**
 * SauceLabs submit delete test
 *
 * @param $form
 * @param $form_state
 */
function saucelabs_test_delete_submit($form, &$form_state) {
  if (!isset($form_state['storage']['delete'])) {
    $form_state['storage']['delete'] = TRUE;
    $form_state['rebuild'] = TRUE;
  }
  else {
    if (isset($form_state['values']['test_id'])) {
      $test = SaucelabsTest::buildFromId($form_state['values']['test_id']);
      $test_name = $test->getName();
      SaucelabsTestDB::delete($test->getTestId());
      drupal_set_message(t('Test %test_name has been removed from the system', array('%test_name' => $test_name)));
      drupal_goto('admin/saucelabs');
    }
  }
}

/**
 * SauceLabs direct Run Test (from URL)
 *
 * @param null $test
 */
function saucelabs_run_test($test = NULL) {
  if (!isset($_GET['cron_key']) || variable_get('cron_key', 'drupal') != $_GET['cron_key']) {
    watchdog('SauceLabs', 'SauceLabs Test could not run because an invalid key was used.', array(), WATCHDOG_NOTICE);
    drupal_access_denied();
  } elseif (isset($test)) {
    $form_state = array();
    $form_state['values']['op'] = 'Run Test';
    $form_state['values']['submit'] = 'Run Test';
    $form_state['values']['test_id'] = $test->getTestId();
    drupal_form_submit('saucelabs_run_test_form', $form_state);
  }
}

/**
 * SauceLabs run test form function to run test via form for cron callback.
 *
 * @param $form
 * @param $form_state
 * @return mixed
 */
function saucelabs_run_test_form($form, &$form_state) {
  $form['test_id'] = array(
    '#type' => 'hidden',
    '#value' => $form_state['values']['test_id'],
  );
  $form['#submit'][] = 'saucelabs_view_test_submit';
  return $form;
}

/**
 * SauceLabs Dashboards.
 */
function saucelabs_dashboard() {
  $s = SaucelabsTest::getSaucelabsApi();
  $profile = '';
  if (isset($s)) {
    $user_details = $s->getAccountDetails();
    $profile = '<h2>SauceLabs Account Details</h2>';
    $profile .= '<p>Username: ' . $user_details['username'] . '</p>';
    if($user_details['monthly_minutes']['automated'] === 0) {
      drupal_set_message(t('It seems the account does not have any monthly minutes for automated tests. Please check your saucelabs account.'));
    } else {
      $profile .= '<p>Test Minutes remaining: ' . $user_details['minutes'] . '</p>';
    }
  }
  else {
    drupal_set_message(t('Please enter your SauceLab details, including location of code, in the settings page'));
  }

  $tests = SaucelabsTest::getAll();
  $display_rows = array();
  $display_header = SaucelabsTest::getListingHeader();
  // Add edit and run links
  $display_header['edit'] = 'Edit';
  $display_header['view'] = 'View';

  foreach ($tests as $test) {
    $links = array(
      l(t('Edit'), 'admin/saucelabs/tests/' . $test->getTestId() . '/edit'),
      l(t('View'), 'admin/saucelabs/tests/' . $test->getTestId())
    );
    $display_rows[] = array_merge($test->getListingRecord(), $links);
  }

  $output = theme('html_tag', array(
    'element' => array(
      '#tag' => 'h2',
      '#value' => 'Configured Tests',
    ),
  ));

  $output .= theme('table', array(
    'header' => $display_header,
    'rows' => $display_rows
  ));

  if (!$location = variable_get('saucelabs_test_location', FALSE)) {
    drupal_set_message("You need to set the locations for your tests.", "error");
  }

  $form['#prefix'] = $profile . $output;

  $form['variables'] = array(
    '#type' => 'fieldset',
    '#title' => t('Manually Run Tests'),
  );

  $form['variables']['full_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Full Path'),
    '#default_value' => isset($_SESSION['saucelabs_full_path']) ? $_SESSION['saucelabs_full_path'] : 'http://' . $_SERVER['HTTP_HOST'],
    '#required' => TRUE,
  );

  $form['tests'] = array(
    '#type' => 'tableselect',
    '#header' => SaucelabsTest::getTestFilesHeaders(),
    '#options' => SaucelabsTest::getTestFilesTableSelect(),
    '#empty' => t('No test files found.'),
    '#attributes' => array(
      'class' => array('saucelabs-test-list'),
    ),
  );

  $form['actions'] = array(
    '#type' => 'item',
  );
  $form['actions']['run'] = array(
    '#type' => 'submit',
    '#value' => 'Run Test(s)',
    '#submit' => array('_saucelabs_batch_run_tests'),
  );

  return $form;
}

/**
 * Prepare environment vars for Batch Job
 *
 * @param $path
 * @return array
 */
function _saucelabs_batch_prep_environment($path) {
  $username = variable_get('saucelabs_username', '');
  $api_key = variable_get('saucelabs_api', '');
  $env_vars = array();
  $php_path = variable_get('saucelabs_php_binary_location', '');
  if (strlen($php_path)) {
    $env_vars[] = 'export PATH=' . $php_path . ':$PATH';
  }
  $env_vars[] = 'export SAUCE_USERNAME=' . $username;
  $env_vars[] = 'export SAUCE_ACCESS_KEY=' . $api_key;
  $env_vars[] = 'export SAUCE_PATH=' . $path;
  return $env_vars;
}

/**
 * Submit Handler: Run the selected test via batch api.
 *
 * @param $form
 * @param $form_state
 */
function _saucelabs_batch_run_tests($form, &$form_state) {
  // Initialize array to store search and replace jobs.
  $jobs = array();

  // Check we have a valid path
  if (isset($form_state['values']['full_path'])) {
    $_SESSION['saucelabs_full_path'] = $form_state['values']['full_path'];
    $path_to_vendor = variable_get('saucelabs_vendor_location', '');
    $threads = variable_get('saucelabs_parallel_threads', 1);
    $env_vars = _saucelabs_batch_prep_environment($form_state['values']['full_path']);
    // Loop through selected tests to create a jobs array.
    if (isset($form_state['values'])) {
      foreach ($form_state['values']['tests'] as $test_file) {
        if ($test_file) {
          $temp_storage = SaucelabsTest::getTempFilePath($test_file);
          $env_vars[] = 'export SAUCE_TMP_SESSION_STORAGE=' . $temp_storage;
          fopen($temp_storage, 'w');
          $prefix = implode(';', $env_vars);
          $test_file = variable_get('saucelabs_test_location', FALSE) . '/' . $test_file;
          $exec_string = $path_to_vendor . '/bin/paratest -p ' . $threads . ' -f --phpunit=' . $path_to_vendor . '/bin/phpunit ' . $test_file . ' 2>&1';
          $jobs[] = $prefix . ';' . $exec_string;
        }
      }
    }
    // If we have jobs to perform.
    if (count($jobs)) {
      // Define the Batch.
      // @see saucelabs.batch.inc for batch functions.
      $batch = array(
        'operations' => array(
          array('saucelabs_process_update', array($jobs, NULL)),
        ),
        'finished' => 'saucelabs_run_tests_finished',
        'title' => t('SauceLabs Batch'),
        'init_message' => t('Batch is starting.'),
        'progress_message' => t('Processed @current out of @total.'),
        'error_message' => t('SauceLabs Batch has encountered an error.'),
        'file' => drupal_get_path('module', 'saucelabs') . '/saucelabs.batch.inc',
      );
      batch_set($batch);
    }
    else {
      drupal_set_message(t("No items selected"), 'error');
    }
  }
}
